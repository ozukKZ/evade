spawn(function()
--
if ESP_LOADED and not _G.ESP_DEBUG == true then
    error("Simple ESP is already running!",0)
    return
end
pcall(function() getgenv().ESP_LOADED  = true end)--
wait(0.5)
--
game.StarterGui:SetCore("SendNotification", {
Title = "Simple ESP by KZ";
Text = "Numpad 3 to hide :3",
Icon = "rbxassetid://10678268125";
Duration = 10;
})
-- Services
local localplayer = game:GetService("Players").LocalPlayer
local uis = game:GetService("UserInputService")
local guiService = game:GetService("CoreGui")

-- Cheat Settings
local cheats = {
    showBorder = true; -- Hiển thị khung viền
    showForceField = false; -- Hiển thị khi có ForceField
    forceFieldTransparency = 1; -- Độ mờ của ForceField
    showDistance = true; -- Hiển thị khoảng cách
    showName = true; -- Hiển thị tên
    displayName = true; -- Hiển thị DisplayName (nếu false thì hiển thị Name)
    showHealth = true; -- Hiển thị thanh máu
    healthType = "Both"; -- Hiển thị loại thanh máu
    showTeam = true; -- Hiển thị ESP cho đồng đội
    teamColor = true; -- Hiển thị màu đội
}

-- GUI Setup
local function createDraggableGui()
    local screenGui = Instance.new("ScreenGui", guiService)
    screenGui.Name = "ESP_Control_UI"
    screenGui.ResetOnSpawn = false

    local mainFrame = Instance.new("Frame", screenGui)
    mainFrame.Size = UDim2.new(0, 200, 0, 355)
    mainFrame.Position = UDim2.new(0, 1500, 0, 50)
    mainFrame.BackgroundColor3 = Color3.fromRGB(50, 50, 50)
    mainFrame.BorderColor3 = Color3.fromRGB(0, 255, 255)
    mainFrame.BorderSizePixel = 2
    mainFrame.Active = true
    mainFrame.Draggable = true

    local titleLabel = Instance.new("TextLabel", mainFrame)
    titleLabel.Size = UDim2.new(1, 0, 0, 30)
    titleLabel.Text = "SIMPLE ESP"
    titleLabel.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
    titleLabel.TextColor3 = Color3.fromRGB(0, 255, 255)
    titleLabel.TextScaled = true

    local toggleButtons = {}

    local function createToggleButton(name, position, callback)
        local button = Instance.new("TextButton", mainFrame)
        button.Size = UDim2.new(1, -10, 0, 20)
        button.Position = position
        button.BackgroundColor3 = Color3.fromRGB(100, 100, 100)
        button.TextColor3 = Color3.fromRGB(255, 255, 255)
        button.Text = name
        button.TextScaled = true
        button.BorderColor3 = Color3.fromRGB(255, 179, 255)
        button.MouseButton1Click:Connect(callback)
        return button
    end

    toggleButtons["Show Border"] = createToggleButton("Show Border", UDim2.new(0, 5, 0, 40), function()
        cheats.showBorder = not cheats.showBorder
        toggleButtons["Show Border"].BackgroundColor3 = cheats.showBorder and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    toggleButtons["Show ForceField"] = createToggleButton("Show ForceField", UDim2.new(0, 5, 0, 80), function()
        cheats.showForceField = not cheats.showForceField
        toggleButtons["Show ForceField"].BackgroundColor3 = cheats.showForceField and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    toggleButtons["Show Distance"] = createToggleButton("Show Distance", UDim2.new(0, 5, 0, 120), function()
        cheats.showDistance = not cheats.showDistance
        toggleButtons["Show Distance"].BackgroundColor3 = cheats.showDistance and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    toggleButtons["Show Name"] = createToggleButton("Show Name", UDim2.new(0, 5, 0, 160), function()
        cheats.showName = not cheats.showName
        toggleButtons["Show Name"].BackgroundColor3 = cheats.showName and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
        -- Sync DisplayName with ShowName
        if not cheats.showName then
            cheats.displayName = false
            toggleButtons["Display Name/Name"].Text = "Display Name: Off"
        end
    end)

    toggleButtons["Display Name/Name"] = createToggleButton("Display Name: On", UDim2.new(0, 5, 0, 200), function()
        if cheats.showName then
            cheats.displayName = not cheats.displayName
            toggleButtons["Display Name/Name"].Text = cheats.displayName and "Display Name: On" or "Display Name: Off"
        end
    end)

    toggleButtons["Show Health"] = createToggleButton("Show Health", UDim2.new(0, 5, 0, 240), function()
        cheats.showHealth = not cheats.showHealth
        toggleButtons["Show Health"].BackgroundColor3 = cheats.showHealth and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    toggleButtons["Show Team"] = createToggleButton("Show Team", UDim2.new(0, 5, 0, 280), function()
        cheats.showTeam = not cheats.showTeam
        toggleButtons["Show Team"].BackgroundColor3 = cheats.showTeam and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    toggleButtons["Team Color"] = createToggleButton("Team Color", UDim2.new(0, 5, 0, 320), function()
        cheats.teamColor = not cheats.teamColor
        toggleButtons["Team Color"].BackgroundColor3 = cheats.teamColor and Color3.fromRGB(0, 255, 0) or Color3.fromRGB(255, 0, 0)
    end)

    return screenGui
end

local gui = createDraggableGui()

-- Toggle GUI visibility with NumPad 3
uis.InputBegan:Connect(function(input, gameProcessed)
    if input.KeyCode == Enum.KeyCode.KeypadThree and not gameProcessed then
        gui.Enabled = not gui.Enabled
    end
end)

-- Create ESP folders
local cheatsf = Instance.new("Folder", guiService)
cheatsf.Name = "cheats"
local espf = Instance.new("Folder", cheatsf)
espf.Name = "esp"

-- Add ESP Function
local function addEsp(player)
    local character = player.Character
    if not character or not character:FindFirstChild("HumanoidRootPart") then return end

    local bbg = Instance.new("BillboardGui")
    bbg.Name = player.Name
    bbg.Parent = espf
    bbg.AlwaysOnTop = true
    bbg.Size = UDim2.new(4,0,5.4,0)
    bbg.ClipsDescendants = false

    local outlines = Instance.new("Frame")
    outlines.Size = UDim2.new(1,0,1,0)
    outlines.BorderSizePixel = 0
    outlines.BackgroundTransparency = 1
    outlines.Parent = bbg
    local left = Instance.new("Frame")
    left.BorderSizePixel = 0
    left.Size = UDim2.new(0,1,1,0)
    left.Parent = outlines
    local right = left:Clone()
    right.Parent = outlines
    right.Size = UDim2.new(0,-1,1,0)
    right.Position = UDim2.new(1,0,0,0)
    local up = left:Clone()
    up.Parent = outlines
    up.Size = UDim2.new(1,0,0,1)
    local down = left:Clone()
    down.Parent = outlines
    down.Size = UDim2.new(1,0,0,-1)
    down.Position = UDim2.new(0,0,1,0)

    local info = Instance.new("BillboardGui")
    info.Name = "info"
    info.Size = UDim2.new(3,0,0,54)
    info.StudsOffset = Vector3.new(3.6,-3,0)
    info.AlwaysOnTop = true
    info.ClipsDescendants = false
    info.Parent = bbg
    local namelabel = Instance.new("TextLabel")
    namelabel.Name = "namelabel"
    namelabel.BackgroundTransparency = 1
    namelabel.TextStrokeTransparency = 0
    namelabel.TextXAlignment = Enum.TextXAlignment.Left
    namelabel.Size = UDim2.new(1,0,0,18)
    namelabel.Position = UDim2.new(0,0,0,0)
    namelabel.Parent = info

    local distancel = Instance.new("TextLabel")
    distancel.Name = "distancelabel"
    distancel.BackgroundTransparency = 1
    distancel.TextStrokeTransparency = 0
    distancel.TextXAlignment = Enum.TextXAlignment.Left
    distancel.Size = UDim2.new(0,100,0,18)
    distancel.Position = UDim2.new(0,0,0,18)
    distancel.Parent = info

    local healthl = Instance.new("TextLabel")
    healthl.Name = "healthlabel"
    healthl.BackgroundTransparency = 1
    healthl.TextStrokeTransparency = 0
    healthl.TextXAlignment = Enum.TextXAlignment.Left
    healthl.Size = UDim2.new(0,100,0,18)
    healthl.Position = UDim2.new(0,0,0,36)
    healthl.Parent = info

    local uill = Instance.new("UIListLayout")
    uill.Parent = info

    local forhealth = Instance.new("BillboardGui")
    forhealth.Name = "forhealth"
    forhealth.Size = UDim2.new(5,0,6,0)
    forhealth.AlwaysOnTop = true
    forhealth.ClipsDescendants = false
    forhealth.Parent = bbg

    local healthbar = Instance.new("Frame")
    healthbar.Name = "healthbar"
    healthbar.BackgroundColor3 = Color3.fromRGB(40,40,40)
    healthbar.BorderColor3 = Color3.fromRGB(0,0,0)
    healthbar.Size = UDim2.new(0.04,0,0.9,0)
    healthbar.Position = UDim2.new(0,0,0.05,0)
    healthbar.Parent = forhealth
    local bar = Instance.new("Frame")
    bar.Name = "bar"
    bar.BorderSizePixel = 0
    bar.BackgroundColor3 = Color3.fromRGB(94,255,69)
    bar.AnchorPoint = Vector2.new(0,1)
    bar.Position = UDim2.new(0,0,1,0)
    bar.Size = UDim2.new(1,0,1,0)
    bar.Parent = healthbar

    local function updateEsp()
        while wait(0.1) do
            if player.Character and player.Character:FindFirstChild("HumanoidRootPart") then
                bbg.Adornee = player.Character.HumanoidRootPart
                info.Adornee = player.Character.HumanoidRootPart
                forhealth.Adornee = player.Character.HumanoidRootPart

                -- Determine if ESP should be shown
                if (player.Team == localplayer.Team and cheats.showTeam) or (player.Team ~= localplayer.Team and cheats.showTeam) then
                    bbg.Enabled = true
                    info.Enabled = true
                    forhealth.Enabled = true
                else
                    bbg.Enabled = false
                    info.Enabled = false
                    forhealth.Enabled = false
                end

                if player.Character:FindFirstChild("ForceField") and cheats.showForceField then
                    outlines.BackgroundTransparency = 0.4
                    left.BackgroundTransparency = 0.4
                    right.BackgroundTransparency = 0.4
                    up.BackgroundTransparency = 0.4
                    down.BackgroundTransparency = 0.4
                    healthl.TextTransparency = 0.4
                    healthl.TextStrokeTransparency = 0.8
                    distancel.TextTransparency = 0.4
                    distancel.TextStrokeTransparency = 0.8
                    namelabel.TextTransparency = 0.4
                    namelabel.TextStrokeTransparency = 0.8
                    bar.BackgroundTransparency = 0.4
                    healthbar.BackgroundTransparency = 0.8
                else
                    outlines.BackgroundTransparency = 0
                    left.BackgroundTransparency = 0
                    right.BackgroundTransparency = 0
                    up.BackgroundTransparency = 0
                    down.BackgroundTransparency = 0
                    healthl.TextTransparency = 0
                    healthl.TextStrokeTransparency = 0
                    distancel.TextTransparency = 0
                    distancel.TextStrokeTransparency = 0
                    namelabel.TextTransparency = 0
                    namelabel.TextStrokeTransparency = 0
                    bar.BackgroundTransparency = 0
                    healthbar.BackgroundTransparency = 0
                end

                if cheats.showBorder then
                    outlines.Visible = true
                else
                    outlines.Visible = false
                end

                if cheats.showForceField then
                    if player.Character:FindFirstChild("ForceField") then
                        outlines.BackgroundTransparency = 0.9
                    else
                        outlines.BackgroundTransparency = cheats.forceFieldTransparency
                    end
                else
                    outlines.BackgroundTransparency = 1
                end

                if cheats.showHealth then
                    local humanoid = player.Character:FindFirstChild("Humanoid")
                    if humanoid then
                        healthl.Text = "Health: " .. math.floor(humanoid.Health)
                        bar.Size = UDim2.new(1,0,humanoid.Health / humanoid.MaxHealth,0)
                    end
                    if cheats.healthType == "Text" then
                        healthbar.Visible = false
                        healthl.Visible = true
                    elseif cheats.healthType == "Bar" then
                        healthl.Visible = false
                        healthbar.Visible = true
                    elseif cheats.healthType == "Both" then
                        healthl.Visible = true
                        healthbar.Visible = true
                    end
                else
                    healthl.Visible = false
                    healthbar.Visible = false
                end

                if cheats.showName then
                    if cheats.displayName then
                        namelabel.Text = "" .. player.DisplayName------------------------------------------------------
                    else
                        namelabel.Text = "" .. player.Name-------------------------------------------------------------
                    end
                    namelabel.Visible = true
                else
                    namelabel.Visible = false
                end

                if cheats.showDistance then
                    distancel.Visible = true
                    if localplayer.Character and localplayer.Character:FindFirstChild("HumanoidRootPart") then
                        local distance = (localplayer.Character.HumanoidRootPart.Position - player.Character.HumanoidRootPart.Position).magnitude
                        distancel.Text = "Distance: " .. math.floor(0.5 + distance)
                    end
                else
                    distancel.Visible = false
                end

                if cheats.teamColor then
                    outlines.BackgroundColor3 = player.TeamColor.Color
                    left.BackgroundColor3 = player.TeamColor.Color
                    right.BackgroundColor3 = player.TeamColor.Color
                    up.BackgroundColor3 = player.TeamColor.Color
                    down.BackgroundColor3 = player.TeamColor.Color
                    healthl.TextColor3 = player.TeamColor.Color
                    distancel.TextColor3 = player.TeamColor.Color
                    namelabel.TextColor3 = player.TeamColor.Color
                else
                    local color = (player.Team == localplayer.Team) and Color3.fromRGB(255, 255, 255) or Color3.fromRGB(255, 0, 0)
                    outlines.BackgroundColor3 = color
                    left.BackgroundColor3 = color
                    right.BackgroundColor3 = color
                    up.BackgroundColor3 = color
                    down.BackgroundColor3 = color
                    healthl.TextColor3 = color
                    distancel.TextColor3 = color
                    namelabel.TextColor3 = color
                end
            end

            if not game:GetService("Players"):FindFirstChild(player.Name) then
                print(player.Name .. " has left. Clearing esp.")
                espf:FindFirstChild(player.Name):Destroy()
                break
            end
        end
    end

    coroutine.wrap(updateEsp)()
end

-- Initialize ESP for existing players
for _, v in pairs(game:GetService("Players"):GetPlayers()) do
    if v.Name ~= localplayer.Name and not espf:FindFirstChild(v.Name) then
        addEsp(v)
    end
end

-- Auto-update ESP
coroutine.wrap(function()
    while wait(6) do
        for _, v in pairs(game:GetService("Players"):GetPlayers()) do
            if v.Name ~= localplayer.Name and not espf:FindFirstChild(v.Name) then
                addEsp(v)
            end
        end
    end
end)()
--
end)--