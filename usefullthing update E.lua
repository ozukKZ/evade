game.StarterGui:SetCore("SendNotification", {
Title = "Evade by kuzo";
Text = "Press V, Event B, ESP mob press N.",
Icon = "rbxassetid://10678266648";
Duration = 20;
})
wait(1)
spawn(function()--||
game.Players.LocalPlayer:GetMouse().KeyDown:Connect(function(key)
    if key == "v" then

game.StarterGui:SetCore("SendNotification", {
Title = "Respawn";
Text = "Onii-chan Okitte!!!~",
Icon = "rbxassetid://10678266648";
Duration = 5;
})
--------------------------------------------------
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local player = Players.LocalPlayer

local character = player.Character or player.CharacterAdded:Wait()

if character:GetAttribute("Downed") then
    ReplicatedStorage.Events.Player.ChangePlayerMode:FireServer(true)
end

--------------------------------------------------
end
end)
end)--||
----------------
wait(1)
spawn(function()--||

-- Dịch vụ cần thiết
local Players = game:GetService("Players")
local Workspace = game:GetService("Workspace")
local RunService = game:GetService("RunService")
local UserInputService = game:GetService("UserInputService")

-- Biến trạng thái để quản lý ESP
local espEnabled = true  -- ESP ban đầu sẽ bật

-- Hàm kiểm tra xem model có phải là người chơi hay không (dựa theo tên)
local function isPlayer(model)
    for _, player in ipairs(Players:GetPlayers()) do
        if player.Name == model.Name then
            return true
        end
    end
    return false
end

-- Hàm tạo ESP cho một NPC (gắn một BillboardGui vào PlayerGui và cập nhật liên tục khoảng cách)
local function createESP(npc, player)
    local hrp = npc:FindFirstChild("HumanoidRootPart")
    if not hrp then return end

    local billboard = Instance.new("BillboardGui")
    billboard.Name = "ESP"
    billboard.Parent = player:WaitForChild("PlayerGui")  -- Đưa BillboardGui vào PlayerGui để tránh ảnh hưởng đến NPC
    billboard.Adornee = hrp  -- Gán Adornee là HumanoidRootPart của NPC
    billboard.AlwaysOnTop = true
    billboard.Size = UDim2.new(0, 100, 0, 25)  -- Nhỏ gọn hơn
    billboard.StudsOffset = Vector3.new(0, 3, 0)  -- vị trí hiển thị trên NPC

    local textLabel = Instance.new("TextLabel")
    textLabel.Parent = billboard
    textLabel.BackgroundTransparency = 1
    textLabel.Size = UDim2.new(1, 0, 1, 0)
    textLabel.TextColor3 = Color3.fromRGB(255, 0, 0)
    textLabel.TextScaled = false
    textLabel.Font = Enum.Font.SourceSansBold
    textLabel.TextSize = 12 -- Cỡ chữ nhỏ gọn hơn
    textLabel.TextStrokeTransparency = 0.5  -- Tạo viền chữ giúp dễ đọc hơn

    -- Hàm cập nhật khoảng cách giữa NPC và người chơi
    local function updateDistance()
        if espEnabled then
            local playerHrp = player.Character and player.Character:FindFirstChild("HumanoidRootPart")
            if playerHrp then
                local distance = (playerHrp.Position - hrp.Position).Magnitude  -- Khoảng cách giữa người chơi và NPC
                textLabel.Text = string.format("%s (%.1f m)", npc.Name, distance)  -- Thêm khoảng cách sau tên NPC
            else
                textLabel.Text = npc.Name  -- Nếu không tìm thấy HumanoidRootPart của người chơi, chỉ hiển thị tên NPC
            end
            billboard.Enabled = true  -- Bật ESP
        else
            billboard.Enabled = false  -- Tắt ESP
        end
    end

    -- Cập nhật liên tục khoảng cách mỗi frame
    RunService.Heartbeat:Connect(updateDistance)
end

-- Lấy folder chứa tất cả các đối tượng NPC và người chơi (trong game có đường dẫn: Workspace.Game.Players)
local npcFolder = Workspace.Game and Workspace.Game:FindFirstChild("Players")
if not npcFolder then
    warn("Không tìm thấy folder Workspace.Game.Players")
    return
end

-- Lấy người chơi hiện tại
local player = Players.LocalPlayer

-- Duyệt qua các đối tượng đã có trong folder để tạo ESP cho các NPC (loại trừ người chơi)
local function initializeESP()
    for _, obj in ipairs(npcFolder:GetChildren()) do
        if not isPlayer(obj) and obj:FindFirstChild("HumanoidRootPart") then
            createESP(obj, player)
        end
    end
end

-- Lắng nghe sự kiện khi có đối tượng mới được thêm vào folder
npcFolder.ChildAdded:Connect(function(obj)
    -- Kiểm tra nếu không phải người chơi
    if not isPlayer(obj) then
        -- Chờ một chút để chắc chắn đối tượng có HumanoidRootPart (tối đa 5 giây)
        local hrp = obj:WaitForChild("HumanoidRootPart", 5)
        if hrp then
            createESP(obj, player)
        end
    end
end)

-- Hàm bật/tắt ESP
local function toggleESP()
    espEnabled = not espEnabled  -- Đảo ngược trạng thái ESP
end

-- Lắng nghe phím "N" để bật/tắt ESP
UserInputService.InputBegan:Connect(function(input, isProcessed)
    if not isProcessed and input.KeyCode == Enum.KeyCode.N then
        toggleESP()
    end
end)

-- Đảm bảo ESP tiếp tục hoạt động sau khi người chơi respawn
player.CharacterAdded:Connect(function()
    initializeESP()  -- Khởi tạo lại ESP khi người chơi respawn
end)

-- Khởi tạo ESP ban đầu
initializeESP()

end)--||
----------------
--tween
wait(1)
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/evade/-/raw/main/Tween.lua"))()
---esp player
wait(1)
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/evade/-/raw/main/Player%20ESP.lua"))()
-----------some event
wait(1)
spawn(function()--||
game.Players.LocalPlayer:GetMouse().KeyDown:Connect(function(key)
    if key == "b" then

game.StarterGui:SetCore("SendNotification", {
Title = "tp to that";
Text = "Onii-chan Goooo~~!",
Icon = "rbxassetid://10678266648";
Duration = 5;
})
--------------------------------------------------
local Players = game:GetService("Players")
local Workspace = game:GetService("Workspace")

local player = Players.LocalPlayer
local tickets = Workspace:FindFirstChild("Game") and Workspace.Game:FindFirstChild("Effects") and Workspace.Game.Effects:FindFirstChild("Tickets")

if tickets then
    local character = player.Character or player.CharacterAdded:Wait()
    local humanoidRootPart = character:WaitForChild("HumanoidRootPart")

    if character and humanoidRootPart then
        local ticketParts = {}  -- Tạo một bảng để chứa tất cả các HumanoidRootPart của vé
        
        -- Lấy tất cả các HumanoidRootPart của các vé
        for _, ticket in ipairs(tickets:GetChildren()) do
            local ticketPart = ticket:FindFirstChild("HumanoidRootPart")
            if ticketPart then
                table.insert(ticketParts, ticketPart.CFrame)  -- Lưu CFrame của vé vào bảng
            end
        end
        
        -- Kiểm tra nếu có vé hợp lệ
        if #ticketParts > 0 then
            -- Dịch chuyển đến vé đầu tiên
            humanoidRootPart.CFrame = ticketParts[1]  -- TP tới tọa độ CFrame của vé đầu tiên

            -- Chờ 0.1 giây trước khi kết thúc quá trình
            task.wait(0.1)
        end
    end
end

--------------------------------------------------
end
end)
end)--||
wait(1)
spawn(function()--||
--
game.StarterGui:SetCore("SendNotification", {
Title = "CHATBOX";
Text = "No Chat box uh!",
Icon = "rbxassetid://10678266648";
Duration = 5;
})
if not game:IsLoaded() then
	local loadedcheck = Instance.new("Message",workspace)
	loadedcheck.Text = 'Loading...'
	game.Loaded:Wait(15)
	loadedcheck:Destroy()
end
local Players, SGui = game:GetService("Players"), game:GetService("StarterGui");
local Client, NColor3, UD, UD2 = Players.LocalPlayer, Color3.new, UDim.new, UDim2.new

local function ChatSpy()
   local ChatSpyFrame = Client.PlayerGui.Chat.Frame
   ChatSpyFrame.ChatChannelParentFrame.Visible = true
   ChatSpyFrame.ChatBarParentFrame.Position = ChatSpyFrame.ChatChannelParentFrame.Position + UD2(UD(), ChatSpyFrame.ChatChannelParentFrame.Size.Y)
end -- brings back chat for games that remove it
ChatSpy()

getgenv().ShowHiddenMsg = function(T, C)
   SGui:SetCore("ChatMakeSystemMessage", {
       Text = T;
       Color = C;
   })
end
getgenv().Spy = function(Target)
   Target.Chatted:Connect(function(Msg)
       if string.find(Msg, "/e ") or string.find(Msg, "/w ") or string.find(Msg, "/whisper ") then
           ShowHiddenMsg("{SPY}: ".."["..tostring(Target).."]: "..Msg, NColor3(255,255,255)) -- https://www.rapidtables.com/web/color/RGB_Color.html if you want to change the color of the hidden msg's
       end
   end)
end

local GP = Players:GetPlayers()
for i = 1, #GP do
   local Plr = GP[i]
   if tostring(Plr) then
       Spy(Plr)
   end
end
Players.PlayerAdded:Connect(function(P)
   if tostring(P) then
       Spy(P)
   end
end)

--
end)--||
wait(1)
-------------------anti afk test-------------------
local Players = game:GetService("Players")
local VirtualUser = game:GetService("VirtualUser")

local player = Players.LocalPlayer

player.Idled:Connect(function()
    VirtualUser:CaptureController()
    VirtualUser:ClickButton2(Vector2.new())
end)
-------------------------------------------
